use criterion::{black_box, criterion_group, criterion_main, Criterion};
use bytes::BytesMut;
use std::sync::Arc;
use object_pool::Pool;

fn bench_without_pool(b: &mut Criterion) {
    b.bench_function("a", |b| {
        b.iter(|| {
            for _ in 0..100_00 {
                BytesMut::with_capacity(8960);
            }
        })
    });
}

fn bench_with_pool(b: &mut Criterion) {
    b.bench_function("b", |b| {
        let pool: Arc<Pool<BytesMut>> = Arc::new(Pool::new(512, || BytesMut::with_capacity(8960)));
        b.iter(|| {
            for _ in 0..100_00 {
                pool.try_pull().unwrap();
            }
        })
    });
}

criterion_group!(benches, bench_without_pool, bench_with_pool);
// criterion_group!(benches, bench_with_pool);
criterion_main!(benches);