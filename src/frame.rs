use bytes::BytesMut;
use byteorder::{BigEndian, ByteOrder};

pub trait QFrame {
    fn id(&self) -> u32;
    fn len(&self) -> u16;
}

impl QFrame for BytesMut {
    fn id(&self) -> u32 {
        BigEndian::read_u32(&self[0..4])
    }

    fn len(&self) -> u16 {
        BigEndian::read_u16(&self[4..6])
    }
}

impl QFrame for [u8] {
    fn id(&self) -> u32 {
        BigEndian::read_u32(&self[0..4])
    }

    fn len(&self) -> u16 {
        BigEndian::read_u16(&self[4..6])
    }
}