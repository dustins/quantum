use std::error::Error;
use std::fmt::{Result, Display, Formatter};

#[derive(Debug)]
pub struct QuantumError {
    message: String
}

impl QuantumError {
    pub fn new(message: &str) -> Self {
        QuantumError {
            message: String::from(message)
        }
    }
}

impl Error for QuantumError {}

impl Display for QuantumError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{}", self.message)
    }
}