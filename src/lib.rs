#![feature(deque_range)]

mod frame;
mod writer;

use std::sync::{Arc};

use bytes::BytesMut;
use object_pool::Pool;
use crossbeam::crossbeam_channel::select;

pub use config::*;
pub use frame::QFrame;


use crate::error::QuantumError;
use crate::stats::Stats;
use std::sync::atomic::Ordering;
use std::time::Instant;

mod reader;
mod error;
mod config;

mod stats;

const MAX_MTU: usize = 8960;

/// Approximately 4MB
const DEFAULT_CHANNEL_SIZE: usize = 468;

pub fn run(config: Arc<Config>) -> Result<(), QuantumError> {
    let (kill_tx, kill_rx) = crossbeam::bounded(1);
    ctrl_channel(kill_tx.clone());

    let pool: Arc<Pool<BytesMut>> = Arc::new(Pool::new(DEFAULT_CHANNEL_SIZE, || BytesMut::with_capacity(MAX_MTU)));
    let stats = Arc::new(Stats::new());

    let start = match config.debug {
        true => Some(Instant::now()),
        false => None
    };

    let (writer_tx, writer_done) = writer::writer(config.clone(), pool.clone(), stats.clone());
    let _reader_done = reader::reader(config.clone(), pool.clone(), writer_tx, stats.clone());

    select! {
        recv(kill_rx) -> _ => {
            println!("Exiting early.");
        },
        recv(writer_done) -> _ => {
            println!("Read  {} bytes", stats.bytes_read().load(Ordering::SeqCst));
            println!("Wrote {} bytes", stats.bytes_written().load(Ordering::SeqCst));
        }
    }

    if let Some(start) = start {
        println!("Completed in {:?}", start.elapsed());
    }

    Ok(())
}

fn ctrl_channel(kill_tx: crossbeam::Sender<()>) {
    ctrlc::set_handler(move || {
        println!("Ctrl-c pressed. Exiting.");
        let _ = kill_tx.send(());
    }).expect("Unable to set Ctrl-c handler.");
}

pub fn bytes_from_pool(pool: &Arc<Pool<BytesMut>>) -> BytesMut {
    let bytes = match pool.try_pull() {
        Some(reusable) => {
            reusable.detach().1
        },
        None => {
            // println!("creating new bytes");
            BytesMut::with_capacity(MAX_MTU)
        }
    };

    bytes
}