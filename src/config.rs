use std::error::Error;
use std::net::SocketAddr;
use std::path::PathBuf;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Config {
    /// Activate debug mode
    #[structopt(short, long)]
    pub debug: bool,

    /// Size of packets to use
    #[structopt(long, default_value = "1024")]
    pub packet_size: usize,

    /// Activate FEC (Forward Error Correction) for input
    #[structopt(long)]
    pub fec_in: bool,

    /// Activate FEC (Forward Error Correction) for output
    #[structopt(long)]
    pub fec_out: bool,

    /// Number of data shards
    #[structopt(long, default_value = "10")]
    pub data_shards: usize,

    /// Number of parity shards
    #[structopt(long, default_value = "2")]
    pub parity_shards: usize,

    /// How many threads to use for reading
    #[structopt(long, default_value = "1")]
    pub reader_threads: usize,

    /// How many threads to use for writing
    #[structopt(long, default_value = "1")]
    pub writer_threads: usize,

    /// Source to read from (example: `file:someFile.txt`, `tcp:192.168.0.2:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "SOURCE", parse(try_from_str = parse_target))]
    pub source: Target,

    /// Destination to send to (example: `file:someFile.txt`, `tcp:192.168.0.2:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "DESTINATION", parse(try_from_str = parse_target))]
    pub destination: Target,
}

impl Config {
    pub fn shards(&self) -> usize {
        self.data_shards + self.parity_shards
    }
}

#[derive(Debug)]
pub enum Target {
    Bench(usize),
    File(PathBuf),
    UDP(SocketAddr),
    TCP(SocketAddr),
}

fn parse_target(target: &str) -> Result<Target, Box<dyn Error>> {
    let delimiter = match target.find(":") {
        Some(index) => {
            index
        }
        None => return Err(Box::<dyn Error>::from("bad format"))
    };

    let (protocol, path) = target.split_at(delimiter);
    match protocol {
        "bench" => {
            Ok(Target::Bench(path[1..].parse::<usize>().unwrap()))
        }
        "tcp" => {
            Ok(Target::TCP(path[1..].parse::<SocketAddr>().unwrap()))
        }
        "udp" => {
            Ok(Target::UDP(path[1..].parse::<SocketAddr>().unwrap()))
        }
        "file" => {
            Ok(Target::File(path[1..].parse::<PathBuf>().unwrap()))
        }
        _ => Err(Box::<dyn Error>::from(format!("Unable to parse `{}`", target)))
    }
}