use structopt::StructOpt;
use quantum::{run, Config};
use std::sync::Arc;

fn main() {
    let config: Arc<Config> = Arc::new(Config::from_args());

    if config.debug {
        println!("Configuration: {:?}", config);
    }

    match run(config) {
        Ok(_) => {},
        Err(e) => panic!("Unable to continue. {}", e)
    }
}
