use std::sync::atomic::AtomicU64;

#[derive(Debug)]
pub struct Stats {
    bytes_read: AtomicU64,
    bytes_written: AtomicU64,

    total_streams: AtomicU64,
    current_streams: AtomicU64,
}

impl Stats {
    pub fn new() -> Self {
        Stats {
            bytes_read: AtomicU64::new(0),
            bytes_written: AtomicU64::new(0),

            total_streams: AtomicU64::new(0),
            current_streams: AtomicU64::new(0),
        }
    }

    pub fn bytes_read(&self) -> &AtomicU64 {
        &self.bytes_read
    }

    pub fn bytes_written(&self) -> &AtomicU64 {
        &self.bytes_written
    }
}