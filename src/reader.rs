use std::cmp::min;
use std::collections::VecDeque;
use std::fs::File;
use std::io::Read;
use std::mem::size_of;
use std::path::PathBuf;
use std::sync::Arc;
use std::sync::atomic::Ordering;
use std::time::Duration;

use bytes::{BytesMut, BufMut};
use crossbeam::{Receiver, Sender};
use object_pool::Pool;

use crate::{bytes_from_pool, Config, DEFAULT_CHANNEL_SIZE, QFrame, Target};
use crate::stats::Stats;
use socket2::{SockAddr, Socket, Domain, Type, Protocol};
use std::net::SocketAddr;
use reed_solomon_erasure::galois_8::ReedSolomon;
use itertools::Itertools;

pub fn reader(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, reader_out: crossbeam::Sender<BytesMut>, stats: Arc<Stats>) -> crossbeam::Receiver<()> {
    let (oneshot_tx, oneshot_rx) = crossbeam::bounded(1);

    // reroute read channel through FEC decoder
    let reader_out = match config.fec_in {
        true => fec_redirect(config.clone(), pool.clone(), reader_out),
        _ => reader_out
    };

    // reroute reader channel through organizer
    let reader_out = match config.source {
        Target::UDP(_) => organizer_redirect(config.clone(), pool.clone(), reader_out),
        Target::File(_) => organizer_redirect(config.clone(), pool.clone(), reader_out),
        _ => reader_out
    };

    spawn_readers(config, pool, reader_out, stats, oneshot_tx);

    oneshot_rx
}

fn organizer_redirect(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, reader_out: Sender<BytesMut>) -> Sender<BytesMut> {
    let (org_tx, org_rx) = crossbeam::bounded(DEFAULT_CHANNEL_SIZE);
    let config = config.clone();
    let pool = pool.clone();

    std::thread::Builder::new()
        .name(format!("Organizer"))
        .spawn(move || {
            match config.fec_in {
                true => organize(config.clone(), pool.clone(), org_rx, reader_out),
                _ => {
                    for bytes in org_rx {
                        reader_out.send(bytes).expect("Unable to send input to writer.");
                    }
                }
            }
        })
        .expect("Unable to spawn Decoder thread.");

    org_tx
}

fn organize(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, org_rx: Receiver<BytesMut>, reader_out: Sender<BytesMut>) {
    let mut queue: VecDeque<BytesMut> = VecDeque::with_capacity(config.shards() * 10);

    'outer: for frame in org_rx {
        let mut insert_index = 0;
        let frame_id = QFrame::id(&frame) as usize;
        for (idx, qframe) in queue.iter().enumerate().rev() {
            if frame_id == (qframe.id() as usize) {
                // return to pool
                pool.attach(frame);
                continue 'outer;
            } else if frame_id > (qframe.id() as usize) {
                insert_index = idx + 1;
                break;
            }
        }

        // insert efficiently
        if insert_index == queue.len() {
            queue.push_back(frame);
        } else {
            queue.insert(insert_index, frame);
        }

        // figure out bounds
        let slice_id = frame_id % config.shards();
        let shard_begin = frame_id - slice_id;
        let shard_end = shard_begin + config.shards() - 1;

        let mut search_begin = 0;
        if slice_id <= insert_index {
            search_begin = insert_index - slice_id;
        }

        let mut search_end = search_begin + config.shards() - 1;
        if search_end >= queue.len() {
            search_end = queue.len();
        }

        // check for shards in chunk
        let mut in_chunk_count = 0;
        let mut first_index = 0;
        let mut last_index = search_begin;
        for (idx, frame) in queue.range(search_begin..search_end).enumerate() {
            let frame_id = frame.id();
            if frame_id as usize > shard_end {
                break;
            } else if frame_id as usize >= shard_begin {
                in_chunk_count += 1;

                if in_chunk_count == 1 {
                    first_index = search_begin + idx;
                }

                last_index = search_begin + idx;
            }
        }

        if in_chunk_count >= config.data_shards {
            queue.drain(first_index..=last_index).for_each(|f| {
                reader_out.send(f).expect("Unable to send to writer.");
            })
        }

        if search_begin > (config.shards() * 8) {
            queue.drain(0..search_begin);
        }
    }
}

fn fec_redirect(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, reader_out: Sender<BytesMut>) -> Sender<BytesMut> {
    let (fec_tx, fec_rx) = crossbeam::bounded(DEFAULT_CHANNEL_SIZE);
    let config = config.clone();
    let pool = pool.clone();

    std::thread::Builder::new()
        .name(format!("Decoder"))
        .spawn(move || fec_decoder(config, pool, fec_rx, reader_out))
        .expect("Unable to spawn Decoder thread.");

    fec_tx
}

fn fec_decoder(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, fec_rx: Receiver<BytesMut>, reader_out: Sender<BytesMut>) {
    let decoder = ReedSolomon::new(config.data_shards, config.parity_shards).unwrap();
    let mut frames: Vec<BytesMut> = Vec::with_capacity(config.shards());
    let mut reconstruct: Vec<(bool, BytesMut)> = Vec::with_capacity(config.shards());
    let frame_size = config.packet_size + 6;

    for bytes in fec_rx {
        // reader_out.send(bytes).expect("Unable to send decoded bytes.");
        frames.push(bytes);
        if frames.len() == config.data_shards {
            let complete = frames.iter().enumerate().fold(true, |acc, f| {
                let id = QFrame::id(f.1.as_ref()) as usize % config.shards();
                acc && id < config.data_shards && f.0 == id
            });

            match complete {
                true => frames.drain(..).for_each(|f| {
                    let len = QFrame::len(&f) as usize;
                    // println!("fec_decode {}", QFrame::id(&f));

                    if len > 0 {
                        let mut bytes = bytes_from_pool(&pool);
                        bytes.clear();
                        bytes.put_slice(&f[6..6+len]);
                        reader_out.send(bytes).expect("Unable to send to writer.");
                    }

                    pool.attach(f);
                }),
                _ => {
                    fill_reconstruct(&config, &pool, &mut frames, &mut reconstruct, frame_size);
                    decode_reconstruct(&config, &decoder, &mut reconstruct);
                    reconstruct.drain(..).dropping_back(2).for_each(|s| {
                        let len = QFrame::len(s.1.as_ref()) as usize;

                        if len > 0 {
                            let mut bytes = bytes_from_pool(&pool);
                            bytes.clear();
                            bytes.put_slice(&s.1[6..6+len]);
                            reader_out.send(bytes).expect("Unable to send to writer.");
                        }

                        pool.attach(s.1);
                    })
                }
            }
        }
    }
}

fn decode_reconstruct(config: &Arc<Config>, encoder: &ReedSolomon, frames: &mut Vec<(bool, BytesMut)>) {
    let mut slices = Vec::with_capacity(config.shards());
    for i in frames {
        slices.push((&mut i.1[4..], i.0));
    }

    encoder.reconstruct_data(&mut slices).expect("Failed to encode frames.");
}

fn fill_reconstruct(config: &Arc<Config>, pool: &Arc<Pool<BytesMut>>, frames: &mut Vec<BytesMut>, reconstruct: &mut Vec<(bool, BytesMut)>, frame_size: usize) {
    let mut last = 0;
    frames.drain(..).enumerate().map(|t| t.1).for_each(|f| {
        let slice_id = QFrame::id(&f) as usize % &config.shards();
        for _ in last..slice_id {
            let mut recovery_shard = bytes_from_pool(&pool);
            recovery_shard.clear();
            recovery_shard.put_u32(8739);
            unsafe { recovery_shard.set_len(frame_size); }
            reconstruct.push((false, recovery_shard));
            last += 1;
        }
        reconstruct.push((true, f));
        last += 1;
    });

    for _ in 0..(config.shards() - reconstruct.len()) {
        let mut recovery_shard = bytes_from_pool(&pool);
        recovery_shard.clear();
        unsafe { recovery_shard.set_len(frame_size); }
        reconstruct.push((false, recovery_shard));
    }
}

fn spawn_readers(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, reader_out: Sender<BytesMut>, stats: Arc<Stats>, oneshot_tx: Sender<()>) {
    // when benchmarking we only want to "read" from 1 thread
    let threads = match config.source {
        Target::Bench(_) => 1,
        Target::File(_) => 1,
        _ => config.reader_threads
    };

    for i in 0..threads {
        let config = config.clone();
        let reader_out = reader_out.clone();
        let pool = pool.clone();
        let oneshot_tx = oneshot_tx.clone();
        let stats = stats.clone();

        std::thread::Builder::new()
            .name(format!("Reader-{}", i))
            .spawn(move || {
                if config.debug {
                    println!("[{}] Spawning Reader", std::thread::current().name().unwrap());
                }

                match &config.source {
                    Target::Bench(bytes_to_read) => {
                        read_bench(config.clone(), pool, *bytes_to_read, reader_out, stats)
                    }
                    Target::File(path) => {
                        read_file(config.clone(), pool, path, reader_out, stats)
                    }
                    Target::UDP(address) => {
                        read_udp(config.clone(), pool, *address, reader_out, stats)
                    }
                    _ => unimplemented!("Not yet implemented.")
                }

                // force move so we can drop it when done
                let _oneshot_tx = oneshot_tx;

                std::thread::sleep(Duration::from_secs(5));
            }).expect("Unable to spawn Reader thread.");
    }
}

fn read_udp(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, address: SocketAddr, reader_out: crossbeam::Sender<BytesMut>, stats: Arc<Stats>) {
    let domain = match address {
        SocketAddr::V4(_) => Domain::ipv4(),
        SocketAddr::V6(_) => Domain::ipv6()
    };

    let socket = Socket::new(domain, Type::dgram(), Some(Protocol::udp())).expect("Unable to create socket.");
    socket.set_reuse_address(true).expect("Unable to set SO_REUSEADDR on socket.");
    socket.bind(&SockAddr::from(address)).expect(&format!("Unable to bind to {:?}", address));

    let read_size = match config.fec_in {
        true => config.packet_size + size_of::<u32>() + size_of::<u16>(),
        _ => config.packet_size
    };

    loop {
        let mut bytes = bytes_from_pool(&pool);
        unsafe { bytes.set_len(read_size); }

        match socket.recv(&mut bytes) {
            Ok(0) => {
                break;
            },
            Ok(bytes_read) => {
                stats.bytes_read().fetch_add(bytes_read as u64, Ordering::SeqCst);
                unsafe { bytes.set_len(bytes_read); }
                reader_out.try_send(bytes).expect("Unable to offload datagram from reader.");
            },
            Err(_e) => panic!("Unable to read socket {:?}", socket)
        }
    }
}

fn read_file(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, path: &PathBuf, reader_out: crossbeam::Sender<BytesMut>, stats: Arc<Stats>) {
    let mut file = File::open(path).expect(&format!("Unable to open {:?}", path));
    let read_size = match config.fec_in {
        true => config.packet_size + size_of::<u32>() + size_of::<u16>(),
        _ => config.packet_size
    };

    loop {
        let mut bytes = bytes_from_pool(&pool);
        unsafe { bytes.set_len(read_size); }

        match file.read(&mut bytes) {
            Ok(0) => break,
            Ok(bytes_read) => {
                stats.bytes_read().fetch_add(bytes_read as u64, Ordering::SeqCst);
                unsafe { bytes.set_len(bytes_read); }
                reader_out.send(bytes).expect("Unable to send bytes from reader.")
            }
            Err(_e) => panic!("Unable to read file {:?}", path)
        }
    }
}

fn read_bench(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, mut bytes_to_read: usize, reader_out: crossbeam::Sender<BytesMut>, stats: Arc<Stats>) {
    while bytes_to_read > 0 {
        let mut bytes = bytes_from_pool(&pool);
        unsafe { bytes.set_len(min(config.packet_size, bytes_to_read)); }

        bytes_to_read -= bytes.len();
        stats.bytes_read().fetch_add(bytes.len() as u64, Ordering::SeqCst);
        reader_out.send(bytes).expect("Unable to send bytes from reader.");
    }
}