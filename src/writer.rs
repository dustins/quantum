use std::sync::Arc;

use bytes::{BytesMut, BufMut};
use object_pool::Pool;

use crate::{Config, Target, DEFAULT_CHANNEL_SIZE, MAX_MTU, bytes_from_pool};
use crate::stats::Stats;
use std::sync::atomic::Ordering;
use std::net::{SocketAddr};
use reed_solomon_erasure::galois_8::{ReedSolomon};
use crossbeam::{Sender, Receiver};
use std::io::Write;
use std::path::PathBuf;
use std::fs::File;
use socket2::{Socket, Type, Domain, Protocol, SockAddr};
use std::time::Duration;

pub fn writer(
    config: Arc<Config>,
    pool: Arc<Pool<BytesMut>>,
    stats: Arc<Stats>) -> (Sender<BytesMut>, Receiver<()>) {
    // create a channel that can signify when all writers are complete
    let (oneshot_tx, oneshot_rx) = crossbeam::bounded(1);

    // create the channel that readers should send data to writers over
    let (writer_tx, writer_rx) = crossbeam::bounded::<BytesMut>(DEFAULT_CHANNEL_SIZE);

    // reroute writer_rx stream through FEC encoder if output is FEC
    let writer_rx = match config.fec_out {
        true => fec_redirect(&config, &pool, writer_rx),
        _ => writer_rx
    };

    // launch writer threads
    spawn_writers(config, pool, stats, oneshot_tx, writer_rx);

    (writer_tx, oneshot_rx)
}

fn fec_redirect(config: &Arc<Config>, pool: &Arc<Pool<BytesMut>>, writer_rx: Receiver<BytesMut>) -> Receiver<BytesMut> {
    let (fec_tx, fec_rx) = crossbeam::bounded(DEFAULT_CHANNEL_SIZE);
    let config = config.clone();
    let pool = pool.clone();

    std::thread::Builder::new()
        .name(format!("Encoder"))
        .spawn(move || fec_encoder(config, pool, writer_rx, fec_tx))
        .expect("Unable to spawn Encoder thread.");

    fec_rx
}

fn spawn_writers(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, stats: Arc<Stats>, oneshot_tx: Sender<()>, writer_rx: Receiver<BytesMut>) {
    // when benchmarking we only want to "read" from 1 thread
    let threads = match config.destination {
        Target::Bench(_) => 1,
        Target::File(_) => 1,
        _ => config.writer_threads
    };

    for i in 0..threads {
        let config = config.clone();
        let pool = pool.clone();
        let writer_rx: Receiver<BytesMut> = writer_rx.clone();
        let oneshot_tx = oneshot_tx.clone();
        let stats = stats.clone();

        std::thread::Builder::new()
            .name(format!("Writer-{}", i))
            .spawn(move || {
                if config.debug {
                    println!("[{}] Spawning Writer", std::thread::current().name().unwrap());
                }

                match &config.destination {
                    Target::Bench(_) => {
                        write_bench(pool, writer_rx, stats);
                    }
                    Target::File(path) => {
                        write_file(pool, writer_rx, path, stats);
                    },
                    Target::UDP(address) => {
                        write_udp(pool, writer_rx, stats, *address)
                    }
                    _ => unimplemented!("Not yet implemented.")
                }

                // force move so we can drop it when done
                let _oneshot_tx = oneshot_tx;
            }).expect("Unable to spawn Writer thread.");
    }
}

fn write_udp(pool: Arc<Pool<BytesMut>>, writer_rx: Receiver<BytesMut>, stats: Arc<Stats>, address: SocketAddr) {
    let socket = Socket::new(Domain::ipv4(), Type::dgram(), Some(Protocol::udp())).expect("Unable to create socket.");
    socket.set_broadcast(true).expect("Unable to set SO_BROADCAST on socket.");
    socket.bind(&"0.0.0.0:0".parse::<SocketAddr>().unwrap().into()).expect("Unable to bind to 0.0.0.0:0");
    socket.connect(&SockAddr::from(address)).expect(&format!("Unable to connect to {:?}", address));

    for slice in writer_rx {
        let sent = socket.send(&slice).unwrap();
        // println!("Sent {} bytes", slice.len());
        stats.bytes_written().fetch_add(sent as u64, Ordering::SeqCst);
        pool.attach(slice);
    }
}

fn fec_encoder(config: Arc<Config>, pool: Arc<Pool<BytesMut>>, writer_rx: Receiver<BytesMut>, fec_tx: Sender<BytesMut>) {
    let encoder = ReedSolomon::new(config.data_shards, config.parity_shards).unwrap();
    let mut frames = Vec::with_capacity(config.shards());
    let frame_size = config.packet_size + 6;

    let mut frame_id = 0;
    for bytes in writer_rx {
        let mut frame = bytes_from_pool(&pool);
        frame.clear();
        frame.put_u32(frame_id);
        frame_id += 1;
        frame.put_u16(bytes.len() as u16);
        frame.put_slice(&bytes);

        // return bytes to pool
        pool.attach(bytes);

        unsafe { frame.set_len(frame_size)}
        frames.push(frame);

        // if we collected enough data shards then encode and send
        if frames.len() == config.data_shards {
            fill_frames(&config, &pool, &mut frames, frame_size, &mut frame_id);
            encode_shards(&config, &encoder, &mut frames);
            send_shards(&fec_tx, &mut frames);
        }

        // todo set timeout to send if no new data is sent
    }

    // send remaining buffered data
    fill_frames(&config, &pool, &mut frames, frame_size, &mut frame_id);
    encode_shards(&config, &encoder, &mut frames);
    send_shards(&fec_tx, &mut frames);
}

fn fill_frames(config: &Arc<Config>, pool: &Arc<Pool<BytesMut>>, frames: &mut Vec<BytesMut>, frame_size: usize, frame_id: &mut u32) {
    for _ in frames.len()..config.shards() {
        let mut parity_shard = pool.pull(|| BytesMut::with_capacity(MAX_MTU)).detach().1;
        parity_shard.clear();
        parity_shard.put_u32(*frame_id);
        *frame_id += 1;
        parity_shard.put_u16(0);
        unsafe { parity_shard.set_len(frame_size); }
        frames.push(parity_shard);
    }
}

fn encode_shards(config: &Arc<Config>, encoder: &ReedSolomon, frames: &mut Vec<BytesMut>) {
    let mut slices = Vec::with_capacity(config.shards());
    for i in frames {
        slices.push(&mut i[4..]);
    }

    encoder.encode(&mut slices).expect("Failed to encode frames.");
}

fn send_shards(fec_tx: &Sender<BytesMut>, frames: &mut Vec<BytesMut>) {
    // frame each encoded slice
    frames.drain(..).for_each(|frame| {
        fec_tx.send(frame).expect("Unable to send FEC results.");
    });
}

fn write_file(pool: Arc<Pool<BytesMut>>, writer_rx: Receiver<BytesMut>, path: &PathBuf, stats: Arc<Stats>) {
    let mut file = File::create(path).expect(&format!("Unable to open {:?}", path));
    for bytes in writer_rx {
        let bytes_written = file.write(&bytes).unwrap();
        stats.bytes_written().fetch_add(bytes_written as u64, Ordering::SeqCst);
        pool.attach(bytes);
    }
}

fn write_bench(pool: Arc<Pool<BytesMut>>, writer_rx: Receiver<BytesMut>, stats: Arc<Stats>) {
    for bytes in writer_rx {
        stats.bytes_written().fetch_add(bytes.len() as u64, Ordering::SeqCst);
        pool.attach(bytes);
    }
}